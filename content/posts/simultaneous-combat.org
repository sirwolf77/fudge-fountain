#+title: Simultaneous Combat
#+date: 2001-10-01T23:31:00+02:00
#+author: Jonathan Benn
#+tags[]: fudge rpg combat rules
#+draft: false

#+BEGIN_aside
by Jonathan Benn. Published originally to [[https://web.archive.org/web/20020615175908/http://www.fudgefactor.org/2001/10/01/simultaneous_combat.html][Fudge Factor]].
#+END_aside

* What is Simultaneous Combat?

Simultaneous Combat is an alternative to the combat system found in the original *Fudge*
rules. It seeks to solve what I perceive as problems with that system.
# more

The original *Fudge* combat rules attempt to pack too much detail into a single die roll,
mainly because the attack roll directly determines damage. This is further exacerbated by
the use of abstract numbers like ODF, DDF and relative degree (RD) to calculate
damage.

In my experience, almost all hits result in either a Hurt or Very Hurt wound. This leaves
out the other three wound types: Scratch, Incapacitated and Near Death.

My design goals in creating Simultaneous Combat were as follows:
- Ease of use
- Using the Fudge 7-tier skill system to determine damage
- More variable damage results

* What kind of combat is Simultaneous Combat designed for?

Simultaneous Combat was created to be used by GMs who like combat that plays through
quickly, and yet provides satisfying results.

It's designed to be used with simultaneous combat rounds, where combatants match up and
attack each other at the same time. Melee combat is resolved as an opposed roll by both
sides. Ranged combat can be slightly more complicated, as it becomes an unopposed roll if
the target is unaware he's being attacked.

GMs who use alternating combat rounds will still find Simultaneous Combat very useful. The
damage system can be used as-is, while the attack system requires only minor modification
(to deal with the fact that each round is divided into attack and defense phases). The
biggest change would be to the optional maneuver rules, although these would also be fairly
easy for a GM to modify to taste.

Simultaneous Combat uses the standard *Fudge* damage levels (shown below). The GM should
continue to use wound boxes exactly as written in the original Fudge rules (i.e. 3 Scratch
boxes, 1-2 Hurt, etc.).

#+CAPTION: Damage Levels
| Damage        | Effect                                        |
|---------------+-----------------------------------------------|
| Scratch       | None                                          |
| Hurt          | -1 to rolls                                   |
| Very Hurt     | -2 to rolls                                   |
| Incapacitated | Out of battle                                 |
| Near Death    | Out of battle, and will die soon without help |

* Simultaneous Combat Basic System

In Simultaneous Combat, battle is split into two rolls. The attack roll determines if a hit
has been scored, while the damage roll determines how much damage has been inflicted.

** The attack roll

Each side makes an opposed roll of the appropriate combat skill. In the case of melee
combat, this is usually one combatant's weapon skill vs. the other's. In ranged combat,
this would typically be the attacker's weapon skill vs. the defender's dodge skill.

In opposed rolls, a RD of 0 is a tie (nothing significant happens), and a RD of 1 or more
is a hit by the victor. In unopposed rolls a RD of 0 or better is a hit. All hits are
assumed to be to the torso. A minimum roll of Poor is required to hit an equal-sized
target.

When someone is being attacked by more than one enemy in melee, the following occurs: the
defender gets a -1 penalty to skill for each attacker after the first. Normally, no more
than 4 attackers of equal size can gang up on someone, making the maximum penalty
-3. Everyone involved in the battle makes an attack roll. If the defender beats every
attacker's roll (penalty included), then he can hit the attacker of his choice. Otherwise,
every attacker who rolled better than the defender hits him.

** The damage roll

Once a victim has been hit by an attack, the attacker makes a damage roll with 4dF. Each
weapon is rated for a Base Damage (BD) value that represents its basic ability to hurt
people. The result of the 4dF roll is added to the weapon's BD to find the amount of damage
inflicted.

*** Base Damage for Primitive Weapons
| Base Damage | Weapon Examples                             |
|-------------+---------------------------------------------|
| Terrible    | Unskilled unarmed                           |
| Poor        | Martial art, brass knuckles                 |
| Mediocre    | Police baton, knife, staff, club            |
| Fair        | Short sword, spear, short bow, baseball bat |
| Good        | Bastard sword, long bow, maul               |
| Great       | Halberd, greatsword                         |

*** Base Damage for Modern Guns
| Base Damage | Weapon Examples                                            |
|-------------+------------------------------------------------------------|
| Mediocre    | .38 Special, .45 ACP, 9mm Parabellum, 7.62 NATO FMJ, AK-47 |
| Fair        | .22 Long, .22 FMJ                                          |
| Good        | 12 gauge shotgun, AK-74, .357 Magnum JSP                   |
| Great       | 7.62 SP, .224 SP                                           |

*** Damage Roll Results
| Degree of Damage Roll | Damage        |
|-----------------------+---------------|
| Terrible-1 or worse   | None          |
| Terrible              | Scratch       |
| Poor                  | Scratch       |
| Mediocre              | Hurt          |
| Fair                  | Very Hurt     |
| Good                  | Incapacitated |
| Great or better       | Near Death    |

** Basic Melee Combat Example

#+BEGIN_ex
A Roman legionnaire is fighting against an unarmored Celtic barbarian. The Roman is armed
with a short sword and has Good skill with it, while the Celt has Great skill with his
broadsword. After both combatants make their attack rolls, the Roman rolls a +1 getting
Great, and the Celt rolls +0 also getting Great, this first round of the combat is a
draw.

During the second round, the Roman gets another +1 for Great, while the Celt gets a -1 for
Good. The Roman has gotten a RD of +1, which is a hit! His short sword's Base Damage is
Fair, he rolls a +1 on 4dF getting a Good result, which means the Celt is now
Incapacitated.
#+END_ex

** Basic Ranged Combat Example

#+BEGIN_ex
A sniper sets up an ambush using a PSG-1 Sniper Rifle (7.62 NATO FMJ). The sniper has Great
skill and his weapon deals Mediocre damage. The sniper takes aim at an enemy Major nearly a
kilometer away. The Major is unarmored and unaware of the attack, so he gets no chance to
defend himself. The GM tells the sniper's player that he'll need a minimum roll of Good to
hit his target in the chest. The sniper's player rolls 4dF for the attack roll, getting -1,
for a rolled degree of Good. Because the target is unaware, a relative degree of 0 still
results in a hit in this case. The sniper rolls a +1 for damage, which gives a Fair or Very
Hurt result.

Seriously wounded, the Major dives for cover. This turn he's entitled to a dodge roll since
he's aware he's being attacked. The sniper fires again, getting a Great roll. The roll is
good enough to hit (a minimum roll of Good is required) unless the major dodges. The major
gets a Good dodge roll. The relative degree between the two rolls is 1, and so the sniper
once again barely scores a hit. This time the sniper rolls a -1 for damage, which results
in a Poor or Scratch result. The second bullet has failed to do any significant
damage. Cursing, the sniper prudently moves on.
#+END_ex

* Simultaneous Combat Advanced System

The following are a series of modular optional rules. They add flavor to combat without
adding much complication. The options are called modular because any or all of them can be
used by the GM in any combination. The GM should use whatever optional rules fit his
personal taste and campaign style.

** Health Rolls to Ignore Penalties

Whenever someone is injured by more than a Scratch, he's entitled to a Health roll. If the
degree of this roll is equal to or greater than the damage degree, then the defender has
"sucked up" the pain. What this means is that the injured person suffers no penalties so
long as his adrenaline level stays high (typically until a few minutes after the action
ends, or a maximum of 15 minutes). Any damage level can be shrugged off, including
Incapacitated and Near Death. Once the action ends, however, the victim immediately suffers
the full effects of his injuries.

** Armor Damage Resistance

The armor a defender is wearing reduces the amount of damage he takes from an attack. This
is called the armor's Damage Resistance (DR). For every level of DR (typically from 1 to
3), the rolled degree for damage is reduced by one level. For instance, DR 2 armor would
reduce an Incapacitated wound result to a Hurt result. Superb+1 damage would be reduced to
Great, which would still result in Near Death.

Primitive armor's DR is still effective against small-caliber pistols. However, it can be
safely ignored by anything larger, such as magnum revolvers, submachine guns and rifles. In
order to be protected from most bullets, a combatant will need modern kevlar armor This
armor comes in three levels (I, II and III), which represent the relative thickness and
heaviness of the armor, level I is the lightest while level III is the heaviest.

Whenever someone wearing kevlar is shot, there is a chance that the bullet will be stopped
entirely. If the bullet isn't stopped, the armor will still reduce the damage. Because
kevlar is flexible, it can never reduce damage below a Scratch. The "dF" column indicates
the number of dF to roll to see if the armor can stop pistol rounds. If all the dice come
up as "-", the armor has not stopped the bullet. Otherwise, the bullet has been stopped and
the wearer only suffers a Scratch. Against submachine guns kevlar gets -1dF, and against
rifle rounds -2dF. If the armor's dF have been reduced to 0 or less, it cannot stop the
bullet. If a round is not stopped, apply the armor's DR to the damage, with the minimum
damage being a Scratch.

Armor piercing (AP) bullets count as one bullet-type higher for the purposes of being
stopped by kevlar (thus AP rifle rounds can't be stopped by infantry armor). They also give
a -1 penalty to the armor's DR. Soft-point (SP) and hollow-point (HP) bullets, while
extremely dangerous to unarmored targets, are very ineffective against armor. Any armor
type (including primitive armor) gets at least 1dF to stop SP/HP bullets, and an extra +2
to DR.

*** Sample Primitive Armor DR
| Armor         | DR |
|---------------+----|
| Leather armor |  1 |
| Chain mail    |  2 |
| Plate mail    |  3 |

*** Sample Modern Armor DR
| Armor             | DR | dF to stop pistols |
|-------------------+----+--------------------|
| Kevlar, level I   |  1 |                  1 |
| Kevlar, level II  |  2 |                  2 |
| Kevlar, level III |  3 |                  3 |

** Armor Passive Defense

Armor has the potential to deflect blows as well as absorb them. This can be reflected by
armor's Passive Defense (PD) trait. The more angled and smooth armor is, the better its PD
should be, since there are fewer locations for weapons to catch, and there's a greater
chance of deflection if the blow is not straight on. Armor PD is played as follows: the
attacker's relative degree must be equal to or greater than the defender's PD in order for
a hit to be scored.

For instance, when fighting someone wearing plate mail (PD 2) who got a Good roll, an
attacker would need to obtain at least a Superb roll in order to hit. The astute reader
will notice that a PD of 1 seems useless, however, it isn't. During unopposed rolls an
attacker normally needs a RD of 0 to hit, whereas if the victim is wearing leather armor
(PD 1) a RD of 1 is required. This can substantially reduce the odds of getting hit.

#+CAPTION: Sample Armor PD
| Armor           | PD |
|-----------------+----|
| Leather, kevlar |  1 |
| Chain mail      |  1 |
| Plate mail      |  2 |

** Strength Damage Modifier

When using this rule, the wielder's Strength attribute affects the damage of melee weapons,
while for bows and crossbows it's the weapon's Strength that affects damage. This rule has
no effect on guns.

|Every +2 to Strength above Fair gives the attack a +1 to damage |
|Every -2 to Strength below Fair gives the attack a -1 to damage |

** Scale Modifiers

When dealing with combatants with different scales, apply the following attack and damage
modifiers. Subtract the defender's scale from the attacker's scale to find out what the
attacker's attack and damage modifiers are. For instance, Tyrannosaurus Rex (scale 10)
would get a -2 to hit and a +5 to damage against human-sized targets.

| Every +4 scale an attacker has gives him a -1 to hit    |
| Every -4 scale an attacker has gives him a +1 to hit    |

| Every +2 scale an attacker has gives him a +1 to damage |
| Every -2 scale an attacker has gives him a -1 to damage |

** Reloading Weapons

Ranged weapons such as bows, crossbows, slings and guns need to be loaded to be used. The
time required to reload them depends on the weapon. Most weapons require one full turn
(approximately 3 seconds) to reload with the following exceptions:

Crossbows require 2 turns to reload if the user's Strength is 1 less than the crossbow's
Strength. If the user's Strength is 2 less than the crossbow's, it takes 6 turns and a
"goat's foot" (a kind of winch) is needed.

Guns with integral (non-removable) magazines require 3 turns to reload.

Revolvers take much longer to reload than guns with removable
magazines. Simultaneously-ejecting revolvers require 3 turns to reload, while
single-ejecting revolvers (where each bullet needs to be added and removed individually)
require a painstaking 6 turns to reload!

Black powder guns are fire-and-forget weapons, they require 5 to 20 turns to reload
(depending heavily on the time period the gun was invented).

** Minimum Strength to Use Weapons

Every weapon has a minimum Strength required to wield it. For most weapons, having Strength
lower than the minimum means that the weapon can't be used. The exceptions are crossbows
and guns, which have reduced effectiveness if the user is too weak to use them
correctly.

*** Melee and Thrown Weapons

The following table describes the minimum Strength required to use these types of
weapons. If the wielder's Strength is lower, the weapon can't be wielded. Using a weapon
with two hands gives the wielder a +1 to effective Strength for the purpose of using the
weapon only (not for damage). For instance, someone with Fair Strength could wield a
baseball bat with two hands, while someone with Good Strength could use it with only one
hand.

#+CAPTION: Minimum Strength Required
| Minimum Strength | Weapon                      |
|------------------+-----------------------------|
| Mediocre         | Police baton, short sword   |
| Fair             | Staff, spear, club          |
| Good             | Bastard sword, baseball bat |
| Great            | Greatsword, maul            |
| Superb           | Halberd, pike               |

*** Bows and Crossbows

Both of these weapon types have Strength of their own. This represents the weight of a bow
(how difficult it is to pull back), or the mechanical power of a crossbow. The weapon's
Strength (and not the wielder's Strength) is what determines a bow or crossbow's damage
modifier. It's always best for someone to have a bow or crossbow tailored to their
Strength.

For bows, if the wielder's Strength is less than the bow's Strength, the person can't use
it. Conversely, if the wielder is stronger than the bow, there is no advantage.

For crossbows, the wielder's Strength is mainly a question of his capacity to reload the
weapon. If the combatant can hold up the crossbow (GM's call for really big crossbows),
then he can fire it. See Reloading Weapons for details.

*** Firearms

Mediocre Strength is required to wield small caliber pistols and rifles. Fair Strength is
required for large caliber pistols, submachine guns and rifles. Good Strength is needed for
really big guns, like miniguns and .50 cal sniper rifles. For every Strength level a gunman
has below the level required, he gets a -1 to skill, because the gun is harder to
control.

** Reduced Damage Variability

For those who think that 4dF provides too much variability to a damage roll, the number of
dice could easily be reduced to 1dF, 2dF, or 3dF depending on the GM and players'
preferences.

** Attack RD Bonus

Many people prefer a system where how well a hit was executed should affect how much damage
was inflicted. In this case, the GM can add a bonus to every damage roll equal to the
attack roll's (RD-1). So a hit by 2 would give a +1 damage bonus, a hit by 3 a +2,
etc. This bonus can easily be adjusted to (RD) or (RD-2), depending on the campaign or GM's
style. GMs using this rule should consider reducing weapons' base damage, as described
below. This rule can also optionally be applied as a damage penalty for low enough RDs. So
if the (RD-2) rule were being used, a RD of 0 would give a -2 damage penalty.

** Modify base damage

There are two ways of increasing/decreasing the deadliness of battles in Simultaneous
Combat:

*** Base Damage by Weapon

Adjusting the base damage of every weapon up or down will directly edit the deadliness of
combat without changing any other factor.

*** Damage Roll Results

Changing this table will affect two things: the deadliness of combat, and the ease of
succeeding Health rolls if that optional rule is in effect.


** Combat Experience

There are times when a GM will want to run a campaign that doesn't include much (or any)
combat. The GM may also want extreme "realism" in combat. The following experience rules
offer an easy way of preventing players from creating combat monsters. All the GM has to do
is require that all characters start his campaign with little or no combat
experience. While characters may improve somewhat over time, their players will be greatly
motivated to avoid battle whenever possible!

There are three levels of combat experience: Green, Fair and Veteran. Someone who's Green
has never (or almost never) been in a fight, and will certainly find large quantities of
adrenaline in his system very disorienting. Someone with Fair experience has been in enough
fights that he can still concentrate during one. A Veteran is just that, a very experienced
combatant who's unlikely to get the jitters during a battle. The process soldiers go
through during boot camp is intended to break civilian reflexes. Soldiers that make it
through boot camp have Fair combat experience.

Whenever an inexperienced fighter's life is at risk (he's being shot at, is currently in a
sword duel, etc.) the disorientation caused by adrenaline and fear reduces the combatant's
skills. A Green fighter's skills are all reduced to Poor for the duration of the fight. All
of a Fair fighter's skills are reduced to Fair. Thus a Green marksman with a Superb skill
on the firing range would only have Poor skill when under fire by the enemy. These skill
caps only apply when the fighter is in danger. An ambusher doesn't receive this penalty
while the element of surprise lasts. As soon as his opponents start fighting back, however,
the skill cap is in force.

After every battle in which a fighter was in danger, he gets to roll Fudge dice to see if
his adrenal reactions have improved. On a successful roll (where all dF come up as "+"),
the character's combat experience is permanently upgraded one level. Soldiers in WWI called
this "seeing the elephant". When a Veteran improves, the GM should give him a gift (such as
Combat Reflexes) that represents improved combat prowess.

#+CAPTION: Combat Experience Table
| Combat Experience | Skill Cap | Improvement |
|-------------------+-----------+-------------|
| Green             | Poor      | +1 on 1dF   |
| Fair              | Fair      | +2 on 2dF   |
| Veteran           | --        | +3 on 3dF   |

** Maneuvers

Maneuvers are actions that a character can take during a combat round. Maneuvers are
usually mutually exclusive (i.e. only one can be used every round). Of course, many other
types of maneuvers are possible (such as dodging, picking up an item, etc.), the ones found
below are only a sampling of a character's options.

** Aim

When using this maneuver, a combatant forfeits his attack this turn in favor of taking
careful aim at his target. This gives him a +1 to hit on the next and subsequent
turns. However, if the character is forced to dodge, or is damaged (and fails the Health
roll) then he loses the bonus. The bonus is also lost if the attacker loses sight of his
target. This maneuver must be used in order to take advantage of a gun's scope. Additional
Aim maneuvers can't be used to increase the hit bonus past +1.

** All-out Attack

This maneuver only applies to melee weapons, bows and automatic pistols. The attacker gets
a +1 to hit and damage, but during his next turn suffers a -2 penalty to any actions,
regardless of the maneuver chosen. The penalty represents the fact that someone
concentrating on an attack is gaining a momentary advantage, but is also opening himself up
to a counter-attack. For automatic pistols, this maneuver uses up 3 rounds of
ammunition.

** All-out Defense

The fighter concentrates on his defense, gaining a +1 to skill, but at the same time he
won't be able to take advantage of his enemy's openings, meaning that if he wins the
opposed roll no damage is inflicted. This maneuver is especially useful for people dodging
missile fire, since they can't hurt their opponent anyway.

** Empty the Clip

This maneuver can only be used with fully automatic guns. Some rifles (such as the M-16)
are equipped with a 3-round cutoff, and cannot be used for this maneuver. Emptying the Clip
is very inaccurate, as each successive shot fired is less likely to hit its target. As
such, Emptying the Clip is no more accurate than a Three-Round Burst, however, the
possibility of more bullets hitting the target is higher. Consequently, this maneuver gives
the attacker a +1 to hit and a +2 to damage, and empties the gun's clip of all its
ammunition (up to a maximum of about 30 rounds). The only major advantage this maneuver has
over Three-Round Burst is that multiple close targets can be hit with it in one
turn. Attacking two targets gives a -1 to hit and damage against both, for three targets
it's a -2 to hit and damage, etc.

** Hit Location

Attacks are normally assumed to hit the torso. A character can use this maneuver to attack
other parts of the body. In general, this maneuver is used to attack a target's vitals,
which results in a -X penalty to the attack roll and gives a +X bonus to damage. The
maximum bonus/penalty is +/- 3. Sometimes attacks will be made to smaller non-vital hit
locations, such as hands or held items. In these cases an attack penalty is applied without
a damage bonus (or with a damage penalty in some cases).

This is a maneuver that can potentially have a lot of depth. First, it is an effective
substitute for the "Attack RD Bonus" optional rule, since it favors characters with high
skill. Second, if the GM is willing, it can be used to produce many special results. For
instance, armor could potentially be bypassed by an attack to its joints, or to unarmored
portions of the body, a warrior could be forced to drop his weapon if his hand is injured,
etc.

*** Sample Hit Location Table

#+CAPTION: *) Attacking armor joints ignores or reduces armor protection
| Hit Location   | Attack | Damage |
|----------------+--------+--------|
| Fingers, toes  |     -3 |     -2 |
| Eyes, arteries |     -3 |     +3 |
| Hands, feet    |     -2 |     -1 |
| Head, heart    |     -2 |     +2 |
| Vitals         |     -1 |     +1 |
| Arms, legs     |     -1 |      0 |
| Armor joint    |     -2 |     0* |

** Suicidal Aggression

This melee maneuver is only used by the most desperate people under the most desperate
circumstances. The person completely ignores his own defense in order to strike his
foe. Suicidal Aggression is gamed as follows: either combatant hits if their attack's
rolled degree is at least equal to the minimum required (including passive defense). For an
equal sized foe, at least a Poor roll is required. If a hit is scored, damage is rolled
normally. Please note that it's likely that both combatants will hit each other
simultaneously... that's the idea.

** Three-Round Burst

This maneuver only applies to fully automatic weapons, such as rifles and submachine
guns. For guns that require three separate pulls of the trigger (such as automatic
pistols), use the All-out Attack maneuver instead. A Three-Round Burst is more accurate
than a single shot because it puts more bullets into the air, and those bullets are fairly
close together. This maneuver gives the user a +1 to hit and damage, at the cost of 3
rounds of ammo.

** Wild Swing

This is a good choice for unskilled attackers, and can only be used in melee. The attacker
gets a +1 bonus to hit, in exchange for a -2 penalty to damage. This maneuver is generally
only available to fighters with weapon skills of less than Fair. This is because more
skilled fighters tend to be more predictable (and effective).


** Advanced Melee Combat Example

In this example the GM is using the "Health Rolls", "Maneuvers" and "Armor DR" optional
rules.

#+BEGIN_ex
A samurai and a tea master are dueling to the death. Both opponents are using broadswords
(Fair damage), wearing leather armor (DR 1), and have Fair Health. The samurai has Good
skill, while the tea master has Poor skill. During the first combat round, the samurai
tests his opponent with a normal attack, while the frightened tea master attempts a Wild
Swing. The samurai rolls a 0, getting a Good roll, while the tea master rolls a +1, which
is a Fair roll, including the maneuver bonus. The samurai has hit his enemy with a relative
degree of 1. He makes a damage roll, getting a -1, which results in a Scratch (Fair, -1 for
roll, -1 for armor, = Poor)

Were this a battle to first blood, it would already be over. However, the combatants
continue to circle each other. The samurai, surprised that his opponent isn't already dead,
takes an All-out Attack in an attempt to finish off his foe. The tea master once again
takes a Wild Swing. The tea master rolls a +2, getting a Good roll with the Wild Swing
bonus. The samurai rolls a -1, resulting in a Good roll thanks to the All-out Attack. This
round is a tie, meaning that the tea master's wild and unpredictable sword swings have
actually foiled the samurai's concerted attack.

Having exposed his flank thanks to his reckless attack, the samurai now suffers a -2
penalty to his roll this round. He wisely uses an All-out Defense in an attempt to reclaim
his position. The tea master uses a normal attack against the other duelist. The samurai,
who seems to be suffering from bad luck, gets a -2 on his roll, resulting in a Poor roll
(Good, -2 All-out Attack, +1 All-out Defense, -2 roll = Poor). The tea master is luckier,
getting a +1, resulting in a Mediocre roll, but enough in this case for a hit! The tea
master makes his damage roll, getting a +3, which results in Near Death (Fair, +3 roll, -1
armor = Great). The samurai succeeds his Health roll with a +2, so he doesn't succumb just
yet.

Bleeding from a horrible cut to his neck, the samurai is enraged. He All-out Attacks again,
while the tea master frantically backs off, taking an All-out Defense. The tables have
finally turned, as the tea master and samurai both roll 0. The tea master gets a Mediocre
roll vs. the samurai's Great, so the tea master is hit by a RD of 3. The samurai gets a +2
for damage, which results in Near Death (Fair, +1 All-out Attack, +2 roll, -1 armor =
Great). The tea master fails his Health roll with a -1. The tea master, having just been
run through the heart, collapses on the ground, followed shortly by the would-be
victor... There is silence in the fields as both fighters die from their wounds.
#+END_ex

** Advanced Ranged Combat Example

In this example the GM is using the "Scale Modifiers", "Maneuvers" and "Armor DR" optional
rules.

#+BEGIN_ex
A hunter wielding a rifle (armed with 7.62 SP rounds) has Good skill and does Great
damage. He takes an Aim maneuver at a nearby deer (scale 4) to give him a +1 to hit. Due to
the animal's size, he gets a +1 to hit, and a -2 to damage. On his next turn the deer is
still unaware of his presence. The hunter decides to shoot for the head, at -2 to hit and
+2 to damage. Overall, his attack and damage modifiers all cancel out to 0. He makes a Good
roll, which is good enough for a hit (according to the GM). The hunter rolls +2 for damage,
which results in major overkill (Superb+1 damage is Near Death). After stumbling around for
a few moments, the deer dies.

Driving home from his expedition, the hunter is driven off the road by a mysterious man in
a pick-up truck. The hunter grabs his gun and dives into the foliage. Peeking out from
behind a tree, he sees that the stranger has followed him into the woods and is wearing a
bulletproof vest (level II kevlar) and carrying a .22 pistol. Not wanting to take the time
to aim, the hunter fires a shot at the man's midsection. The GM judges that the hunter's
roll of Fair is a hit at this range. The hunter rolls -1 on 4dF, giving him a damage roll
of Good. Since this is a Soft-Point round, the stranger's kevlar gets 1dF to stop the
bullet, and has DR 4. The stranger rolls a "-" so the shot gets through, however, its
damage is reduced to Terrible (a Scratch!).

Aware of his victim's location, the stranger fires three times in the hunter's
direction. For an automatic pistol this is an All-out Attack. Despite his +1 to hit the
stranger misses with all three shots (the GM explains that it's because the tree offers the
hunter much-needed cover). On his next turn, the hunter decides that he needs to end this
quickly, not the least reason being he only has one bullet left in his rifle. He decides to
take a chance and fire at the stranger's gun hand. This shot is at -1 to hit and -1 to
damage. The hunter gets a +1 on 4dF, which is a Good roll and a hit. He rolls a +2 for
damage, resulting in Superb or Near Death damage. The GM decides that the stranger's hand
has been blown clean off, but that the actual damage is only Very Hurt since this is an
extremity. Holding his arm's stump to staunch the blood, the stranger leaps into his truck
and awkwardly drives away. The hunter breathes a sigh of relief.
#+END_ex
