#+title: Fudge Abstract Funds
#+date: 2004-06-01T13:37:04+02:00
#+tags[]: fudge rpg character rules
#+draft: false

#+BEGIN_aside
by Duke York. Published originally to [[https://web.archive.org/web/20040921090929/http://www.fudgefactor.org/2004/06/01/funds.html][Fudge Factor]].
#+END_aside

* Introduction

From its earliest beginnings, roleplaying has been about acquisition.  Whether it's a
simple lust for gold or a more altruistic desire to fund an escalating campaign against
ever more pernicious evils, most great heroes are peddlers at heart, searching their loot
for whatever gives the most plusses and trading the rest for gain.

# more

Unfortunately, this horse-trading can slow down the actual business of the game -
adventuring. Fudge can offer a quick, easy way to keep things flowing smoothly by assigning
Terrible to Superb levels to monetary values. When you expand on this, you can have an
entire game where the players never have to write down a single monetary value. Even
better, the GM never has to detail how many gold pieces are in the dragon's hoard or how
many credits the zaibatsu's secret databanks are worth.

* Assets

Everything a character possesses, from a laser-armed space destroyer to an individual
copper farthing, is an asset whose value can be found on the following chart:


| Fudge Level | Modern U.S. Dollars |
|-------------+---------------------|
| Terrible    | $1 -- $5            |
| Poor        | $6 -- $25           |
| Mediocre    | $26 -- $125         |
| Fair        | $126 -- $625        |
| Good        | $626 -- $3,125      |
| Great       | $3,126 -- $15,625   |
| Superb      | $15,626 -- $78,125  |

And so on.

Notice that each level is roughly five times the previous level. This means that five
assets of one value are equal in value to one of the next highest level.  It also means
that most attempts at bargaining under this system are bound to failure; if someone offers
you a Good price for a used car (around $2,000), you're not going to be able to haggle them
up to a Great price (around $10,000). If you want to trade to fixed-value assets, you'll
need to find to assets that both parties agree are in the same price range, or,
alternatively, gather more than one asset.


* Liquid Assets

Fixed assets (such as pots and horses) have one major disadvantage; they can't be divided
into smaller assets without changing them completely. This leads to the absurdities of the
barter system (/"your change is three chickens and a duck"/) and huge losses of
value (trading a Good value sword for a Mediocre value cart horse because you need to
ride). To solve these problems and facilitate trade, societies around the world have
invented liquid assets.  The most obvious liquid asset is simple cash, but checking
accounts, lines of credit and bags of gold dust can be divided and traded in small parts in
much the same way. Even silos full of grain or tankers full of oil are liquid assets
because their owners can split them into tiny amounts.

Liquid assets have values the same as fixed ones, so a purse full of copper and silver
might be a Poor-value liquid asset, while a money-market account might be a Good asset. The
advantage of the liquid asset is that you can buy a mug of ale or a laptop and still have
value left over.

When you buy a fixed asset with a liquid asset, if the fixed asset is the same value of the
liquid one, you get the fixed asset and a liquid asset of one value less. (This is your
change; if you use your Good value money market to buy a Good value Laptop, you have a Fair
value money market.)

If the fixed asset you're purchasing is of lower value than the liquid asset, then make a
check against the value of the fixed asset. If that check is higher than the value of the
liquid asset, the liquid asset goes down one level.

Of course, if the liquid asset is of lower value than the fixed one, it's impossible to buy
it.

** Example

#+BEGIN_ex
Cromrad, the Barbarian, is carrying all of his worldly possessions with him, including a
purse with a Good amount of cash. Unfortunately, these possessions do not include a sword,
which he lost slaying the great Sandbourne Beast. His first stop in the decadent eastern
city in which he finds himself is to purchase new arms; one of those strange curved swords
everyone here is carrying costs a Good amount; Cromrad's purse has a Fair amount remaining
in it.

Next Cromrad spends the night drinking and wenching. Considering the types of taverns that
our hero frequents, the GM decides this is costs a Mediocre amount. Cromrad's player (or
the GM) rolls a Mediocre versus a difficulty of Fair. When a +2 comes up on the dice, the
value of Cromrad's purse drops down to Mediocre and Cromrad needs to find some loot, fast.
#+END_ex

If you add a liquid asset to one you already have, roll against the value of the liquid
asset you're adding. If the result is larger than the old asset, that old asset goes up one
level in value. This works the same if you're adding an asset of the same size, as well;
you need to roll a +1 when adding together two Fairs to get a Good, for example.

If you somehow manage to find a liquid asset that's larger than your current funds, you can
add your current funds to your new amount instead. This is another of the great benefits of
liquid assets.

** Example

#+BEGIN_ex
After a little sword-point negotiating, Cromrad takes the purses off of two low-level
thugs. He finds, to his disgust, that they're worse off than he is; they only have Poor
assets between them. His player rolls a -1 when he combines their purses; he'd need to roll
a +2 (getting degree of Fair) to increase liquid assets from Mediocre to Fair.

He decides to take a bigger risk next; he follows rumors of a lost temple buried in the
sand, protected by unspeakable ancient evils. What he finds out in the desert he never
says, but he does return with a ruby the size of a polver's egg, which he pawns for a Great
amount of money. Since he's combining the two amounts of money, he can roll when he adds
his old purse to the new windfall; he lucks out and gets a +4, meaning that the few coppers
and silvers he had with him was enough to push the money from the gem up into the Superb
range
#+END_ex

* Conclusion

With these skills, you should be able to do just about every basic economic transaction characters need. You can buy, sell and trade, and never have to write down a number.  If you want to stretch yourself, you can find other uses as well. Perhaps you could model experience with a liquid asset, so that when a character performs a heroic feat her player has to roll to see if she learned anything from it. Or, perhaps a wizard's store of magical power is a liquid asset, doled out to work spells with a straight Fudge difficulty. As with Fudge itself, the possibilities are endless.
