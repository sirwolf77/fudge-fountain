#+title: A Non-Linear Wounding System
#+date: 2004-05-05T22:49:00+02:00
#+author: Helge Lund Kolstad
#+tags[]: fudge rpg combat wounds rules
#+draft: false

#+BEGIN_aside
By Helge Lund Kolstad. Published originally to [[https://web.archive.org/web/20041014001157/http://www.fudgefactor.org/2004/05/05/nonlinear.html][Fudge Factor]].
#+END_aside

Most RPG damage systems have a linear way of regarding wounds, i.e.  hit points, a wound
track, or a similar mechanism. The idea is that if you sustain enough small wounds, you
will eventually die. This is fine for most games, but is nevertheless somewhat
unrealistic. I present here an optional damage system, inspired by Fantasy Flight Games'
Synergy game system.

The system is based on the assumption that you don't die if you sustain several non-mortal
wounds. You might be hindered, feel bad and look unwell, but you won't die from it unless
your condition somehow worsens. Likewise, it's fully possible to die from a single wound,
no matter how tough you are.
# more

* The Health Conditions
The conditions a person can be in is connected to how much damage he has sustained, but
there is no direct correlation. You might stumble around and fight back, even if you're
spilling entrails and slipping in your own blood. Or you might be concussed and out like a
light, but otherwise fine.

- Healthy :: Pretty self-explanatory. The subject is not hindered by any wounds he might
  have sustained.
- Hindered :: The subject has sustained some wounds, and is feeling it. An arm might be
  numb, a blow might have caused dizziness; in any case, this is reflected by a penalty to
  all rolls one makes.
- Stunned :: The subject is reeling from pain and shock and takes a round of ouch
  time. This is a temporary condition. While stunned, one might stumble away from immediate
  danger and dodge a few blows, but is otherwise incapable of coherent action. Defence is
  at -2.
- Incapacitated :: The subject is out cold, but his or her condition is not
  life-threatening. No action can be taken.
- Dying :: The subject is down and has sustained serious body trauma. He can take no
  action and will die unless the injury is treated. How long that takes depends on the
  injury and how nasty the GM is feeling; however, it is not too late...
- Dead :: But now it is. In a fantasy or science-fiction campaign, it /might/ be possible
  to raise the dead, in which case the body's condition may be a factor.

* The Wound Levels

This is of course what it all comes down to. When playing, players might keep track of
wounds by keeping differently-coloured counters. For example, the GM might give them a
glass bead for each wound, or the player might fasten laquered paper clips to the character
sheet.

The wound levels describe:
- The amount of damage sustained from the attack that caused the wound.
- The Resistance roll. When a wound is sustained, one rolls against a relevant trait;
  Damage Capacity, Health, and Resolve are all good choices. The GM decides what he likes
  best. This roll is not affected by any wound penalties.
- The penalty. This how much each wound hinders the subject. All penalties are cumulative,
  except as noted above.

#+HTML: <div class="bigtable">
| Type            | Damage | Nature                                                                                                                                                                                                                                                           | Roll                                                                                                                                                                                                                                                                   | Penalty                                                                                                          |
|-----------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------|
| Flesh Wound     |    1-3 | This might be a graze, a flesh wound, a sprained ankle, or something similar. The injury is painful and might hinder action, but is not very serious.                                                                                                            | No roll is needed for flesh wounds. The subject grunts/screams in pain/does not flinch (whatever is appropriate) and is otherwise unaffected.                                                                                                                          | The subject receives a -1 penalty for every two of these babies. Only one flesh wound will not hinder one. Much. |
| Severe Wound    |    4-8 | This is something more noticable - actual fractures, a punctured lung, a concussion, or something similar. The subject is severely hurt and needs medical attention, but the injury is not in itself life-threatening.                                           | If the subject fails the roll, he is *Stunned* for one round. Success means he grits his teeth and fights on. The difficulty is Good.                                                                                                                                  | The subject receives a -1 penalty for /every/ severe wound sustained.                                            |
| Critical Wound  |   9-14 | These are life-threatening injuries, usually because the subject's vital organs have been damaged. His stomach might be cut open and his guts spilling out (always a bad sign).                                                                                  | Failing the roll means the subject is *Dying*. Success means he is still able to act; however, he might be permanently crippled at the GM's discretion. The difficulty is Great.                                                                                       | For sustaining this kind of injury, the subject earns himself a -2 penalty.                                      |
| Traumatic Wound |    15+ | This is the kind of damage that can kill you in an instant - standing within the blast range of an exploding bomb, for example. The subject will likely be crippled for the rest of his life, and should count himself lucky to survive. If he can count at all. | Failure means the subject is instantly, messily *Dead*. Even if he succeeds, he will be *Dying* and barely alive. The roll has a Superb difficulty. For every 3 damage above 15, another level is added to difficulty (Legendary at 18, Legendary+1 at 21, and so on). | A traumatic wound incurs a -3 penalty to all rolls.                                                              |
#+HTML: </div>

* Infection
Even after the wound is sustained, the danger is not over. For every day spent wounded,
there is a risk that the wound might be infected. At the end of each day, the subject rolls
against Fair (use Health, Damage Capacity or similar trait. Resolve might not be as
relevant here). Failure means the wound has become infected.

The roll is influenced by any first aid administered to the subject. The environment might
have adverse effects as well. In a jungle, for example, wounds are likely to fester.

If a wound is infected, its severity goes up by one level, i.e. a flesh wound becomes a
severe wound. The subject then has to succeed at a Resistance roll or suffer the
consequences of the new wound.  It is possible for a wound to be affected by infection more
than once as the infection gets worse.

* Treating Wounds
** Healing
If a wound is allowed to heal normally, its severity will decrease by one level every two
weeks. Hospitalisation will reduce this time by half. The Gift Rapid Healing will further
reduce it by half.  Even an infected wound will get better. Yes, it is unrealistic for a
bomb victim to be completely healed after four weeks in hospital, but this is where some
realism has to be sacrificed for playability.

** First Aid
A subject who is *Dying* will, well, die without immediate medical care. A successful First
Aid roll will change the subject's condition to *Incapacitated* and put him out of
immediate danger. If he gets sufficient rest, he might wake up and be merely *Hindered*;
however, the wound level is not reduced by first aid.

* Other Nastiness
Being wounded might have other consequenced than that mentioned above. The subject might be
cut by a poisoned blade, creating a seemingly innocuous wound that will not heal and
gradually gets worse as per infection. A particularly grievous wound might cause the loss
of a limb, an eye, or other body part. This is all up to the GM, but playability should be
kept in mind. It might be appropriate for someone who survived a direct hit from a 19th
century cannon to lose a leg or an arm, or both, but players might be a bit miffed from
losing an hand to a severe wound in a cinematic campaign.

* Examples

#+BEGIN_ex
Ugly Joe is in an old-style Wild West duel with El Hombre. Fingers twitch as the clock atop
the church tower approaches 12. The two shots ring as one. Finally, Joe makes a gurgling
sound and sinks to his knees.
#+END_ex

In this example, Joe gets a Fair Revolver skill result, while El Hombre gets a Superb
one. El Hombre's revolver has a damage rating of 7, while Joe's heavy duster gives him 1
armour. Total damage is 9, which is a critical wound. Ugly Joe now has to make a Resolve
roll against Great difficulty, but rolls only Good. He falls over and is now dying.

#+BEGIN_ex
Bad Luck Betty has been in a firefight, and taken three hits. One merely grazed her
arm. One passed through her side, but failed to hit any vital organs. The third bullet,
however, entered very close to her throat and has shattered her collarbone.  Although in
pain, she bit the bullet and managed to escape with her life. Bandaging her wounds, she
decides to cross the desert and search for help in the first town she can find.
#+END_ex

The first two hits are flesh wounds, but the second one is obviously a severe wound. Betty
made the Resistance roll, however.  Two flesh wounds mean a -1 penalty to all rolls, and
the severe wound another -1, for a total of -2. She has made her First Aid roll, but it
will have no effect other than lessening the chance of infection.

#+BEGIN_ex
Jean-Pierre Brisset and his partner Pascal Dufresne are searching the Amazon jungle for the
Lost Idol of Quetzalcoatl when they are surprised by hostile natives and forced to
flee. Jean-Pierre has taken two massive spear wounds while Pascal has only a relatively
innocuous wound from a blowpipe dart.  Of course, as seasoned explorers they both know
about the danger of poisoned darts, so Jean-Pierre desperately tries to suck the poison
out. However brave, the attempt to alleviate Pascal's pain fails, and Jean-Pierre has to
watch his partner die only a few hours later. The next day, one of Jean-Pierre's own wounds
hurts considerably more and is looking rather worse. He gets a fever and considers his own
prospects pretty bleak. However, Lady Luck smiles upon him as he is rescued by a friendly
tribe just as he is about to pass out.
#+END_ex

In this example, Jean-Pierre has sustained two severe wounds and Pascal only a flesh
wound. However, Pascal's wound is poisoned and the GM decides Pascal's player has to make a
Health check every hour against Good difficulty. Pascal fails the first roll, which is bad
since his next roll will be modified for a severe wound, not a flesh wound. Meanwhile,
Jean-Pierre tries to suck out the poison, which the GM rules will be a Medicine roll
against Superb difficulty to shake the poison effects of the wound. Jean-Pierre doesn't
even have the Medicine skill, so he fails. Although Pascal lasts for a few more hours, he
ultimately fails the poisoning roll and the critical wound roll, and Jean-Pierre can only
watch his dying partner writhe in agony.


Meanwhile, the day passes and it's time for Jean-Pierre to make his infection roll. He is
in a damp jungle environment with lots of insects and bacteria to make his day worse, so
the GM decides the roll will have a Superb difficulty. However the wounds have been treated
with Pascal's First Aid skill, so the difficulty is lowered to Great. Jean-Pierre is in
Good Health; however, his two severe wounds lowers his effective rating to Mediocre. His
first two rolls are Great and Fair, which means one wound stays uninfected while an
incredibly nasty South American insect lays its eggs in the other.  Jean-Pierre now has one
severe and one critical wound.


The French explorer shambles along all day and night, and it's time for infection rolls
again. This time, his effective skill is Poor (Good -1 for the severe wound, -2 again for
the critical one), and the difficulty is still Great. The rolls are Terrible and Fair - not
even close. Jean-Pierre now has one critical and one traumatic wound. Although he fails the
Resistance roll for the critical wound, he succeeds with the traumatic one, so he doesn't
die outright, but passes out as his life slowly seeps away.  Fortunately, he is found just
in time for treatment.

#+BEGIN_ex
Ted the ninja trainee has already become legendary for his ineptitude and bad luck. One day
during katana practice, he trips and skewers himself on his own sword. His sensei carries
him to the hospital, shaking his head. Florence the ninja nurse takes very good care of
him, so what seemed at first a rather horrid wound quickly heals. After only two weeks, a
rather embarrassed Ted is ready to resume practice. He has, however, fallen in love with
Florence, which will cause him no end of trouble.
#+END_ex

In this example, Ted first sustains a severe wound from his own sword. He is quickly
carried to hospital, so one wound level will be healed every week. He still has to make
infection rolls, but in this clean environment the difficulty will be Terrible. Even with
Ted's Mediocre Health, he passes with flying colours. After one week, his injury has
receded to a flesh wound, and after two, it is completely healed. He /has/ acquired
heartache, though, which is not covered by this system.


#+BEGIN_ex
By the cover of night, "Dead Meat" Hamish and the other soldiers wade ashore from the
landing boats. They have been making silent progress across the landscape when the soldier
next to Hamish suddenly steps on a mine. Hamish is flung 10 metres in the air and lands
brutally, alive but not looking it. Groaning painfully, he rolls onto his back - and
triggers another mine.  Again, he is flung several meters across a ridge and lands in front
of an enemy truck. He is then run over. As Hamish is lying there, smeared across the road,
He thinks "not again!" as he loses consciousness.
#+END_ex

This is an example of multiple wounds. As his mate is killed by a mine, Hamish sustains 16
damage, which is a traumatic wound.  Hamish's player rolls Health against Superb difficulty
and makes it - the unlucky soldier shielded most of the blast. Then, Hamish is blasted by
another mine - a direct hit. This time it's 20 damage and Legendary+1 difficulty. Since
Hamish has Great Health, The player wins it again with another lucky roll. The GM rules
Hamish hit the trigger with his knee, and his left leg is blasted off. He is then run over
by a military truck, crushed for 17 damage by several tonnes of vehicle. His chest and most
of his internal organs are crushed, but his heart still lurches on, since the Resistance
difficulty is "only" Superb. The player can be glad no wound penalties are applied to the
Resistance roll or he would be in trouble. Hamish is still very much dying and in enemy
territory, though.
