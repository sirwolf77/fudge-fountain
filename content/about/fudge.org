#+title: Fudge
#+draft: false

Fudge is a roleplaying game written by Steffan O’Sullivan, with extensive input from the
Usenet community of rec.games.design and other online forums. The core rules of Fudge are
available free on the Internet at [[http://www.fudgerpg.com]] and other sites.

Fudge was designed to be customized, and may be used with any gaming genre. Fudge
gamemasters and game designers are encouraged to modify Fudge to suit their needs, and to
share their modifications and additions with the Fudge community. The Fudge game system is
copyrighted ©2000, 2005 by Grey Ghost Press, Inc., and is available for use under the Open
Game License. See the [[http://fudgerpg.com/][fudgerpg.com]] website for more information.
